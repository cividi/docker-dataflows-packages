# Start from a simple python image
FROM twsmartuse/python-dataflows:latest

# Install mapping support
RUN pip install mapboxgl psycopg2-binary geopandas
RUN pip install openrouteservice

# Install pandas and geopandas
RUN pip install pandas
RUN pip install geopandas

# Install plotting librariers
RUN pip install matplotlib

# Install additional ML tools
RUN pip install TextBlob TextBlob-de